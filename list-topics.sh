#!/bin/bash -e

COMMAND="${KAFKA_HOME}/bin/kafka-topics.sh \\
	--list \\
	--zookeeper ${KAFKA_ZOOKEEPER_CONNECT}"

eval "${COMMAND}"

wait
